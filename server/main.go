package main

import (
	proto2 "github.com/golang/protobuf/proto"
	"github.com/streadway/amqp"
	proto "gitlab.com/konfka/review5/proto"
	"log"
)

func main() {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		log.Fatalf("Failed to connect to RabbitMQ: %v", err)
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		log.Fatalf("Failed to open a channel: %v", err)
	}
	defer ch.Close()

	orderQueue, err := ch.QueueDeclare(
		"order_request_queue",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf("Failed to declare a queue: %v", err)
	}

	statusQueue, err := ch.QueueDeclare(
		"order_status_queue",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf("Failed to declare a queue: %v", err)
	}

	msgsOrder, err := ch.Consume(
		orderQueue.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf("Failed to consume messages: %v", err)
	}

	channel := make(chan bool)

	go func() {
		for msg := range msgsOrder {
			var order proto.Order

			if err = proto2.Unmarshal(msg.Body, &order); err != nil {
				log.Printf("Failed to unmarshal protobuf: %v", err)
				continue
			}

			log.Printf("Received order message: %+v", &order)
			status := proto.Status{
				OrderId: order.OrderId,
				Success: true,
				Message: "Ordered successfully",
			}
			bytesStat, err := proto2.Marshal(&status)
			if err != nil {
				log.Fatalf("Error marshalling status, %e", err)
			}
			err = ch.Publish("", statusQueue.Name, false, false, amqp.Publishing{
				ContentType: "application/octet-stream",
				Body:        bytesStat,
			})
			if err != nil {
				log.Fatalf("Error to publish data, %e", err)
			}
		}
	}()
	log.Println("Waiting for orders...")
	<-channel
}
