module gitlab.com/konfka/review5

go 1.20

require (
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/streadway/amqp v1.1.0 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
