package main

import (
	proto2 "github.com/golang/protobuf/proto"
	"github.com/streadway/amqp"
	proto "gitlab.com/konfka/review5/proto"
	"log"
	"time"
)

func main() {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		log.Fatalf("Failed to connect to RabbitMQ: %v", err)
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		log.Fatalf("Failed to open a channel: %v", err)
	}
	defer ch.Close()

	orderQueue, err := ch.QueueDeclare(
		"order_queue",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf("Failed to declare a queue: %v", err)
	}

	statusQueue, err := ch.QueueDeclare(
		"order_status_queue",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf("Failed to declare a queue: %v", err)
	}

	order := proto.Order{
		OrderId:     2,
		ProductName: "Guitar",
		Quantity:    1,
		TotalPrice:  1500.0,
		Client:      123643,
	}

	body, err := proto2.Marshal(&order)
	if err != nil {
		log.Fatalf("Failed to marshal protobuf: %v", err)
	}

	err = ch.Publish("", orderQueue.Name, false, false, amqp.Publishing{
		ContentType: "application/octet-stream",
		Body:        body,
	})
	if err != nil {
		log.Fatalf("Error to publish data, %e", err)
	}

	msgsStatus, err := ch.Consume(
		statusQueue.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf("Failed to consume messages: %v", err)
	}

	go func() {
		for msg := range msgsStatus {
			status := proto.Status{}
			err = proto2.Unmarshal(msg.Body, &status)
			if err != nil {
				log.Fatalf("Error marshalling status, %e", err)
			}
			log.Printf("Status:, %v", &status)
		}
	}()
	time.Sleep(2 * time.Second)
}
